import * as d3 from "d3";

function splitmix32(a: number) {
	return function () {
		a |= 0;
		a = (a + 0x9e3779b9) | 0;
		let t = a ^ (a >>> 16);
		t = Math.imul(t, 0x21f0aaad);
		t = t ^ (t >>> 15);
		t = Math.imul(t, 0x735a2d97);
		return ((t ^ (t >>> 15)) >>> 0) / 4294967296;
	};
}

function cyrb128(str: string) {
	let h1 = 1779033703;
	let h2 = 3144134277;
	let h3 = 1013904242;
	let h4 = 2773480762;
	for (let i = 0, k; i < str.length; i++) {
		k = str.charCodeAt(i);
		h1 = h2 ^ Math.imul(h1 ^ k, 597399067);
		h2 = h3 ^ Math.imul(h2 ^ k, 2869860233);
		h3 = h4 ^ Math.imul(h3 ^ k, 951274213);
		h4 = h1 ^ Math.imul(h4 ^ k, 2716044179);
	}
	h1 = Math.imul(h3 ^ (h1 >>> 18), 597399067);
	h2 = Math.imul(h4 ^ (h2 >>> 22), 2869860233);
	h3 = Math.imul(h1 ^ (h3 >>> 17), 951274213);
	h4 = Math.imul(h2 ^ (h4 >>> 19), 2716044179);
	(h1 ^= h2 ^ h3 ^ h4), (h2 ^= h1), (h3 ^= h1), (h4 ^= h1);
	return [h1 >>> 0, h2 >>> 0, h3 >>> 0, h4 >>> 0];
}

export function Random(seed = "") {
	const s = cyrb128(seed);
	return splitmix32(s[0]);
}

export function createPolygon(cities: any[]) {
	try {
		let coordinates = [];
		for (let i = 0; i < cities.length - 1; i++) {
			const start = cities[i].coordinates;
			const end = cities[i + 1].coordinates;
			const interpolate = d3.geoInterpolate(start, end);
			for (let t = 0; t <= 1; t += 0.01) {
				coordinates.push(interpolate(t));
			}
		}
		const start = cities[cities.length - 1].coordinates;
		const end = cities[0].coordinates;
		const interpolate = d3.geoInterpolate(start, end);
		for (let t = 0; t <= 1; t += 0.01) {
			coordinates.push(interpolate(t));
		}

		const area =
			d3.geoArea({ type: "Polygon", coordinates: [coordinates] }) > 2 * Math.PI;
		if (area) {
			coordinates = coordinates.reverse();
		}

		return {
			type: "Polygon",
			coordinates: [coordinates],
		};
	} catch (error) {
		console.error("unable to create polygon", cities, error);
	}
}
