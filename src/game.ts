import * as d3 from "d3";
import d3GeoZoom from "d3-geo-zoom";
import * as topojson from "topojson";

import world from "./data.json";
import capitals from "./capitals.ts";
import { createPolygon, Random } from "./utils";

const settingsModalEl = document.getElementById(
	"settingsDialog",
) as HTMLDialogElement;
document.getElementById("openSettings")?.addEventListener("click", (e) => {
	settingsModalEl?.showModal();
});
document.getElementById("closeSettings")?.addEventListener("click", (e) => {
	settingsModalEl?.close();
});

settingsModalEl?.addEventListener("click", (e) => {
	const rect = settingsModalEl.getBoundingClientRect();
	if (e.clientY < rect.top || e.clientY > rect.bottom) {
		return settingsModalEl.close();
	}
	if (e.clientX < rect.left || e.clientX > rect.right) {
		return settingsModalEl.close();
	}
});

const allCapitals = capitals.features.filter(
	(feature) => feature.properties.city,
);

const cities = allCapitals.map((feature) => ({
	name: feature.properties.city,
	coordinates: feature.geometry.coordinates,
})) as Array<{ name: string; coordinates: [number, number] }>;

const cityFeatures = allCapitals.map((feature) => ({
	type: "Feature",
	properties: { name: feature.properties.city },
	geometry: { type: "Point", coordinates: feature.geometry.coordinates },
}));

const numFormat = Intl.NumberFormat();

type GameState = {
	selectedCities: Array<{ name: string; coordinates: [number, number] }>;
	drawLines: boolean;
	drawDistance: boolean;
	drawAllCities: boolean;
	initialTransition: boolean;
};

const gameState: GameState = {
	selectedCities: [],
	drawLines: false,
	drawDistance: false,
	drawAllCities: false,
	initialTransition: window.location.hash === "",
};

function render(
	ctx: CanvasRenderingContext2D,
	{
		selectedCities = [],
		drawDistance = false,
		drawLines = false,
		drawAllCities = false,
	}: GameState,
) {
	ctx.clearRect(0, 0, +canvas.attr("width"), +canvas.attr("height"));

	ctx.shadowColor =
		localStorage.getItem("theme") === "dark"
			? "rgba(255, 255, 255, 0.5)"
			: "rgba(0, 0, 255, 0.3)";
	ctx.shadowBlur = 20;
	ctx.shadowOffsetX = 0;
	ctx.shadowOffsetY = 0;

	ctx.beginPath();
	path({ type: "Sphere" });
	ctx.fillStyle = "#eee";
	ctx.fill();

	ctx.shadowColor = "transparent";
	ctx.shadowBlur = 0;
	ctx.shadowOffsetX = 0;
	ctx.shadowOffsetY = 0;

	ctx.beginPath();
	// @ts-expect-error
	path(topojson.feature(world, world.objects.land));
	// @ts-expect-error
	svgPath(topojson.feature(world, world.objects.land));
	ctx.fillStyle = "#333";
	ctx.fill();

	if (drawAllCities) {
		ctx.beginPath();
		path({
			type: "MultiPoint",
			coordinates: cities.map((city) => city.coordinates),
		});
		ctx.fillStyle = "rgba(0, 0, 255, 0.2)";
		ctx.lineWidth = 1;
		ctx.fill();
	}

	if (selectedCities.length > 0) {
		for (const city of selectedCities) {
			ctx.beginPath();
			const cityFeature = cityFeatures.find(
				(cityFeature) => cityFeature.properties.name === city.name,
			);

			// @ts-expect-error
			path(cityFeature);
			ctx.fillStyle = "rgba(255, 0, 0, 0.8)";
			ctx.strokeStyle = "black";
			ctx.lineWidth = 1;
			ctx.stroke();
			ctx.fill();

			if (svgPath({ type: "Point", coordinates: city.coordinates })) {
				if (drawDistance) {
					ctx.beginPath();
					const distanceInKm =
						d3.geoDistance(city.coordinates, randomCity.coordinates) * 6371;
					const distanceInDegrees = distanceInKm / 111.32;
					const circle = d3
						.geoCircle()
						.center(city.coordinates)
						.radius(distanceInDegrees);

					// @ts-expect-error
					path({ type: "Feature", geometry: circle() });
					ctx.strokeStyle = "rgba(0, 0, 255, 0.5)";
					ctx.lineWidth = 2;
					ctx.stroke();

					ctx.fillStyle = "rgba(0, 0, 255, 0.05)";
					ctx.fill();
				}

				const [x, y] = projection(city.coordinates) ?? [0, 0];
				const cityX = x + 5;
				const cityY = y - 5;
				ctx.beginPath();
				ctx.font = "16px 'system-ui', 'Arial', sans-serif";
				ctx.strokeStyle = "black";
				ctx.lineWidth = 1;
				ctx.lineJoin = "miter";
				ctx.miterLimit = 1;
				ctx.strokeText(city.name, cityX, cityY);
				ctx.fillStyle = "red";
				ctx.fillText(city.name, cityX, cityY);
			}
		}

		if (cities.length > 1 && drawLines) {
			const polygon = createPolygon(selectedCities);
			ctx.beginPath();
			// @ts-expect-error
			path(polygon);
			ctx.strokeStyle = "rgba(255, 0, 0, 0.5)";
			ctx.lineWidth = 2;
			ctx.stroke();
		}
	}
}

function getCanvasSize() {
	const worldContainer = document.getElementById("world-container");
	const width = worldContainer?.clientWidth || 0;
	const height = worldContainer?.clientHeight || 0;
	return [width, height];
}

const [width, height] = getCanvasSize();
const canvas = d3
	.select("#world")
	.append("canvas")
	.attr("width", width)
	.attr("height", height);

const ctx = canvas.node()?.getContext("2d") as CanvasRenderingContext2D;

const projection = d3
	.geoOrthographic()
	.scale(Math.min(width, height) / 2 - 5)
	.translate([width / 2, height / 2]);

window.addEventListener("resize", () => {
	const [width, height] = getCanvasSize();
	canvas.attr("width", width).attr("height", height);
	projection.translate([width / 2, height / 2]);
	render(ctx, gameState);
});

const path = d3.geoPath().context(ctx).projection(projection);
const svgPath = d3.geoPath().projection(projection);

if (!window.location.hash) {
	window.location.hash = btoa(Math.random() + "");
}

const rand = Random(window.location.hash);

const randomCity = cities[Math.floor(rand() * cities.length)];
console.log(randomCity);

const otherCities = cities.filter((city) => city.name !== randomCity.name);
const otherCity = otherCities[Math.floor(rand() * otherCities.length)];

d3GeoZoom()
	.projection(projection)
	.northUp(true)
	.onMove(() => render(ctx, gameState))(canvas.node());

render(ctx, gameState);

for (const city of cities) {
	const option = document.createElement("option");
	option.value = city.name;
	option.text = city.name;
	document.getElementById("city-selection")?.appendChild(option);
}

function selectCity(state: GameState, selectedCityName = "") {
	if (state.selectedCities.find((city) => selectedCityName === city.name)) {
		return;
	}

	state.selectedCities.push(
		...cities.filter((city) => selectedCityName === city.name),
	);

	const selectedCitiesEl = document.getElementById("selected-cities");
	if (!selectedCitiesEl) {
		return;
	}

	selectedCitiesEl.innerHTML = state.selectedCities
		.map((city, idx) => {
			if (randomCity.name === city.name) {
				return `<li>${city.name} 🎉</li>`;
			}

			const distance =
				d3.geoDistance(city.coordinates, randomCity.coordinates) * 6371;
			return `<li>${city.name} (${numFormat.format(Math.round(distance))} km${
				idx === 0 ? " from target city" : ""
			})</li>`;
		})
		.join("");

	let centroid = [0, 0];
	for (let city of gameState.selectedCities) {
		centroid[0] += city.coordinates[0] / gameState.selectedCities.length;
		centroid[1] += city.coordinates[1] / gameState.selectedCities.length;
	}

	const currentRotation = projection.rotate();
	const finalRotation = [-centroid[0], -centroid[1], 0];
	const currentScale = projection.scale();

	let maxDistance = 0;
	for (let i = 0; i < gameState.selectedCities.length; i++) {
		for (let j = i + 1; j < gameState.selectedCities.length; j++) {
			const point1 = gameState.selectedCities[i].coordinates.map(
				(d) => (d * Math.PI) / 180,
			);
			const point2 = gameState.selectedCities[j].coordinates.map(
				(d) => (d * Math.PI) / 180,
			);

			const dLon = point2[0] - point1[0];
			const dLat = point2[1] - point1[1];
			const a =
				Math.sin(dLat / 2) ** 2 +
				Math.cos(point1[1]) * Math.cos(point2[1]) * Math.sin(dLon / 2) ** 2;
			const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			const distance = 6371 * c;

			maxDistance = Math.max(maxDistance, distance);
		}
	}

	const UPPER_BOUND_DISTANCE = 10_000;
	maxDistance = Math.min(maxDistance, UPPER_BOUND_DISTANCE);

	const scaleLinear = d3
		.scaleLinear()
		.domain([0, UPPER_BOUND_DISTANCE])
		.range([1000, 300]);

	const finalScale = Math.abs(scaleLinear(maxDistance)) - 50;

	d3.transition()
		.duration(
			state.selectedCities.length > 1 || state.initialTransition ? 1000 : 0,
		)
		.tween("render", () => {
			const rotationInterpolator = d3.interpolate(
				currentRotation,
				finalRotation,
			);
			const scaleInterpolator = d3.interpolate(currentScale, finalScale);
			return (t) => {
				projection.rotate(rotationInterpolator(t) as [number, number, number]);
				projection.scale(scaleInterpolator(t));
				render(ctx, gameState);
			};
		})
		.on("end", () => {
			if (selectedCityName === randomCity.name) {
				alert("You guessed the right city!");
				document.location.hash = "";
				document.getElementById("city-selection-form")?.remove();
			}
		});
}

document
	.getElementById("city-selection-form")
	?.addEventListener("submit", (event) => {
		event.preventDefault();
		const input = document.getElementById("city-input") as HTMLInputElement;
		const selectedCityName = input.value;
		input.value = "";
		selectCity(gameState, selectedCityName);
	});

function renderCheckbox(
	state: GameState,
	stateProperty: "drawLines" | "drawDistance" | "drawAllCities",
) {
	if (!stateProperty) {
		return;
	}

	const checkbox = document.getElementById(stateProperty) as HTMLInputElement;
	checkbox.checked = window.location.search.includes(`${stateProperty}=true`);
	state[stateProperty] = checkbox.checked;
	checkbox.addEventListener("change", (event) => {
		state[stateProperty] = (event.target as HTMLInputElement).checked;
		const newUrl = new URL(document.location.href);
		newUrl.searchParams.set(stateProperty, `${state[stateProperty]}`);
		const newUrlString = newUrl.toString();
		window.history.pushState({ path: newUrlString }, "", newUrlString);
		render(ctx, state);
	});
}

renderCheckbox(gameState, "drawLines");
renderCheckbox(gameState, "drawDistance");
renderCheckbox(gameState, "drawAllCities");

selectCity(gameState, otherCity.name);
