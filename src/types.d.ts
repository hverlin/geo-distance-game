declare module "d3-geo-zoom" {
	interface D3GeoZoom {
		(): D3GeoZoom;

		projection(projection: any): D3GeoZoom;

		northUp(northUp: boolean): D3GeoZoom;

		onMove(callback: () => void): (canvas: HTMLCanvasElement | null) => void;
	}

	const d3GeoZoom: D3GeoZoom;
	export default d3GeoZoom;
}
