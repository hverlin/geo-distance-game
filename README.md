# Geo-distance game

### Install dependencies
```shell
npm install -g pnpm
pnpm i
```

### Serve
```shell
pnpm start
```

### Build
```shell
pnpm build
```